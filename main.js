// lilfish the smallest one with size=1 and value=40
// killfish is the medium one with size=2 and value=30
// fatfish is the biggest one with size=3 and value=20
const WIDTH = 800;
const HEIGHT = 600;

let game_box=document.createElement('div');
game_box.className='game_box';
game_box.style.width=WIDTH+'px';
game_box.style.height=HEIGHT+'px';
document.body.appendChild(game_box);

let login_box = document.createElement('div');
login_box.className="login_box";

let input_first = document.createElement('input');
input_first.setAttribute('type', 'text');
input_first.id="name_input";
input_first.setAttribute('placeholder', "Введите имя");
input_first.required = true; 

let input_second = document.createElement('input');
input_second.setAttribute('type', 'button');
input_second.setAttribute('value', 'Старт');
let message = document.createElement('h3');
input_second.onclick= function(){
	if (input_first.value==="")
		message.innerHTML = "Please, enter your name";
	else{
		game_data.name = document.getElementById('name_input').value;
		game_box.removeChild(login_box);
		start_game();
	}

};

let finish_box = document.createElement('div');
finish_box.id="finish_box";

let restart = document.createElement('a');
restart.id="restart";
restart.innerHTML="Play again";
restart.onclick = function(){
/*	game_box.innerHTML='';
	start_game();*/
	document.location.reload(true);
}

login_box.appendChild(input_first);
login_box.appendChild(input_second);
login_box.appendChild(message);
game_box.appendChild(login_box);

let score_box = document.createElement('div');
score_box.className="score-box";

let blur_screen=document.createElement('div');
blur_screen.id="blur_screen";

let countdown = document.createElement('h1');

function Fish(name, x, y, size, speed, way){
	this.name = name;
	this.y = y;
	this.x  =x;
	this.newX = 0;
	this.newY = 0;
	this.size = size;
	this.speed = speed;
	this.way = way;
	this.turned = false;

	switch(size){
		case 1: this.value = 40;
				this.width = 20;
				this.height = 69;
			break;
		case 2: this.value = 30;
				this.width = 120;
				this.height = 70;
			break;
		case 3: this.value = 20;
				this.width = 60;
				this.height = 60;
			break;
	}

	let l=document.createElement('div');
	l.setAttribute("balls", this.value);
	l.className='killfish';

	l.onclick=function(){
		l.className='dead_fish';
		let cost = l.getAttribute('balls');
		game_data.score+=+cost;
		// console.log(game_data);
	}
	game_box.appendChild(l);
	this.l = l;

};
/*let vodim = new Fish('Vodim', 0, 0, 2 , 18, 'Gerard');
console.log(vodim);*/

function fishBirth(){
	let number = getRandom(1, 3);
	let fish = new Fish('Vodim', 0, 0, number , 18, 'Gerard');
	return fish;
}

Fish.prototype.AI = function(){
	let maxX = getRandom(100, WIDTH-this.width);
	let maxY = getRandom(100, HEIGHT-this.height);
	// let maxX = 0;
	// let maxY = 300;
	this.newX = maxX;
	this.newY = maxY;

	// console.log('sex: '+this.newX+' '+this.newY);
}

Fish.prototype.moveToXY = function(speed, distanceX, distanceY){
// console.log(distanceX, distanceY);
this.l.style.transition='transform '+speed+'s ease';
let a=this.l.style.transform;
// console.log(a);
if (this.turned) this.l.style.transform=' translate('+distanceX+'px, '+distanceY+'px)';
else this.l.style.transform=' translate('+distanceX+'px, '+distanceY+'px)';

// this.l.classList.toggle("killfish-left");
}
// Fish.prototype.moveToY = function(speed, distance){
// 	this.l.style.transition='all '+speed+'s ease-out 0.5s';
// 	this.l.style.transform = 'translateY('+distance+'px)';
// }
Fish.prototype.setOldPos = function(){
	this.x = this.newX;
	this.y = this.newY;
}
Fish.prototype.rotateFish = function(){
	// this.l.style.transform = 'scale(-1,1)';
	// this.l.style.transition='transform .5s ease';
	this.l.classList.add('turned-fish');
	this.turned = true;
}
Fish.prototype.defaultFish = function(){
	// this.l.style.transform = 'scale(1,1)';
	// this.l.style.transition='transform .5s ease';
	this.l.classList.remove('turned-fish');
	this.turned =false;
}
Fish.prototype.distinguishURWay = function(){
	// console.log(this);
	// console.log(this.x, this.newX+" X и NewX");
	if ((this.x<this.newX)  && (this.y ==this.newY)){
		// console.log("right");
		this.rotateFish();
		// let a = this.l.style.transform;
		// a += " scale(-1, 1)";
		// this.l.style.transform=a;
	}
	if ((this.x<this.newX) && (this.y < this.newY)){
		// console.log("rghtbtm");
				this.rotateFish();
		// let a = this.l.style.transform;
		// a += " scale(-1, 1)";
		// this.l.style.transform=a;
	}
	if ((this.x == this.newX) && (this.y < this.newY)){
		// console.log("btm");
		this.defaultFish();
	}
	if ((this.x > this.newX) && (this.y < this.newY)){
		// console.log("btmleft");
		this.defaultFish();
	}
	if ((this.x > this.newX) && (this.y == this.newY)){
		// console.log("left");
		this.defaultFish();
	}
	if ((this.x > this.newX) && (this.y > this.newY)){
		// console.log("lefttop");
		this.defaultFish();
	}
	if ((this.x == this.newX) && (this.y > this.newY)){
		// console.log("top");
		this.defaultFish();
	}
	if ((this.x < this.newX) && (this.y > this.newY)){
		// console.log("topright");
		this.rotateFish();
		// let a = this.l.style.transform;
		// a += " scale(-1, 1)";
		// this.l.style.transform=a;
	}
}
function getRandom(min, max){
	return Math.floor(Math.random()*(max-min+1))+min;
}
function bestest(){
let winners_table = [];
for (var i = 0; i < localStorage.length; i++){
	// console.log(i);
    let person_score = localStorage.key(i);
        // console.log(i);
    console.log(person_score);
    let person_name = localStorage.getItem(person_score);
        console.log(person_name);
    let winner = {
    	score: person_score,
    	name: person_name
    }
    winners_table.push(winner);
    winners_table.sort(function(a,b){
    	return +a.score < +b.score ? 1 : -1;
    });

}
console.log(winners_table);
print_best_results(winners_table);
}
function print_best_results(obj){
	
	console.log(obj);
	let h = document.createElement("h2");
	h.innerHTML = "Best of the best:";
	let wrap = document.createElement("div");
	wrap.id = "wrapper";
	// let els = [];
	for (lil_obj of obj){
		// console.log(lil_obj);/
/*		let obob = obj[lil_obj];
		els.push(obob);*/	
		let bi = document.createElement('div');
		bi.id = "winner";
		console.log(lil_obj);
		bi.innerHTML = lil_obj.name+" счёт "+lil_obj.score;
		wrap.appendChild(bi);
	}
	document.getElementById('finish_box').appendChild(h);
	document.getElementById('finish_box').appendChild(wrap);
/*	els = els.sort();
	for (each_els in els){
		let an = document.createElement('div');
		an.id = "winner";
		an.innerHTML=each_els;
	}*/
}

let game_data = {name: "", score: 0, time: 10};		
let timeouting = 5;

function start_game(){
	game_box.appendChild(blur_screen);

		var first_interval = setInterval(function(){
		timeouting-=1;
		// blur_screen.innerHTML = '<span>'+timeouting+'</span>';
		countdown.innerHTML = timeouting;
		game_box.appendChild(countdown);
		if (timeouting<0){
			clearInterval(first_interval);
			game_box.removeChild(countdown);
			game_box.removeChild(blur_screen);
			game_box.appendChild(score_box);
			var game_interval = setInterval(function(){
				game_data.time-=1;
				score_box.innerHTML ='<span> Your name: '+ game_data.name + '</span><span> Score: '+ game_data.score + '</span><span> Time: '+ game_data.time+'s</span>';
				if (game_data.time<0){
		 			clearInterval(game_interval);
		 			game_box.innerHTML='';
		 			finish_box.innerHTML = '<span> Your name: '+ game_data.name + '</span><span> Score: '+ game_data.score + '</span>';
		 			localStorage.setItem(game_data.score, game_data.name);
		 			game_box.appendChild(finish_box);
		 			bestest();
		 			finish_box.appendChild(restart);
				};
			}, 1000);
		};
	}, 1000); 

	
	

fishes=[];
let number_of_fishes = getRandom(6, 10);
for (let i=1; i<number_of_fishes; i++){
	fishes.push(fishBirth());
}

var timeOld = 5;
var timeNew = 5;

setInterval(function(){
	timeNew = getRandom(2, 5);

	for(fish of fishes){
		// console.log(fish);
		fish.AI();
		fish.distinguishURWay();
		fish.moveToXY(timeOld, fish.newX, fish.newY);
		fish.setOldPos();
	}
	// console.log("old: "+ timeOld);

	/*timeNew = getRandom(2, 5);
	// Добавляет newX и NewY
	vodim.AI(); 
	// vodim.rotateFish();
	vodim.distinguishURWay();
	// setTimeout(vodim.moveToXY(timeOld, vodim.newX, vodim.newY), 10000)
	vodim.moveToXY(timeOld, vodim.newX, vodim.newY);
		// vodim.moveToXY(timeOld, 200, 0);
	vodim.setOldPos();*/

	timeOld = timeNew;
	// console.log("young "+ timeNew);
},  timeOld*700)
}

// setInterval(function(){
// 		console.log("worked11Q!");
// 		vodim.distinguishURWay();
// 		vodim.setOldPos();

// }, timeOld*1000-500);
